package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RookTest {

    private RulesOfGame rook;
    private Point2d pointFrom;
    private Point2d pointTo;

    @BeforeEach
    public void initDataForRook() {

        rook = new Rook();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForRook() {

        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 10);
        assertTrue(rook.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(-10, -1);
        pointTo = new Point2d(10, -1);
        assertTrue(rook.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(-1, -1);
        pointTo = new Point2d(5, 5);
        assertFalse(rook.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(1, 1);
        pointTo = new Point2d(-1, -2);
        assertFalse(rook.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        assertFalse(rook.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);
    }
}
