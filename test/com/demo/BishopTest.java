package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BishopTest {

    private RulesOfGame bishop;
    private Point2d pointFrom;
    private Point2d pointTo;

    @BeforeEach
    public void initDataForRook() {

        bishop = new Bishop();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForBishop() {

        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(20, 20);
        assertTrue(bishop.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(-1, -1);
        pointTo = new Point2d(-21, -21);
        assertTrue(bishop.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(-1, 4);
        pointTo = new Point2d(-3, 2);
        assertTrue(bishop.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(0, 1);
        pointTo = new Point2d(2, -1);
        assertTrue(bishop.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(0, 1);
        pointTo = new Point2d(1, -1);
        assertFalse(bishop.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        assertFalse(bishop.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);
    }
}
